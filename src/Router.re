type page =
  | Settings
  | Login
  | Index;

type route = page;

let toRoute = (url: ReasonReact.Router.url) =>
  switch (url.path) {
  | ["settings"] => Settings
  | ["login"] => Login
  | [] => Index
  | _ => Index
  };

let toUrl = page =>
  switch (page) {
  | Settings => "settings"
  | Login => "login"
  | Index => ""
  };

type renderProps = {
  updateRoute: route => unit,
  route,
};
type state = {route};
type action =
  | UpdateRoute(page);

let component = ReasonReact.reducerComponent("Router");

let make = (~render, _children) => {
  ...component,
  initialState: () => {
    route: ReasonReact.Router.dangerouslyGetInitialUrl() |> toRoute,
  },
  subscriptions: self => [
    Sub(
      () =>
        ReasonReact.Router.watchUrl(url =>
          self.send(UpdateRoute(toRoute(url)))
        ),
      ReasonReact.Router.unwatchUrl,
    ),
  ],
  reducer: (action, _state) =>
    switch (action) {
    | UpdateRoute(route) => ReasonReact.Update({route: route})
    },
  render: self =>
    render({
      updateRoute: route => self.send(UpdateRoute(route)),
      route: self.state.route,
    }),
};