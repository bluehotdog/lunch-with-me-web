[%bs.raw {|require('./App.css')|}];

[@bs.module] external logo : string = "./logo.svg";

let component = ReasonReact.statelessComponent("App");

let make = _children => {
  ...component,
  render: _self =>
    <ReasonApollo.Provider client=Client.instance>
      <Router
        render=(
          ({route}) =>
            switch (route) {
            | Login => <LoginPage />
            | Settings => <div> (ReasonReact.string("settings")) </div>
            | Index => <div> (ReasonReact.string("index")) </div>
            }
        )
      />
    </ReasonApollo.Provider>,
};