module SignIn = [%graphql
  {|
     mutation sigsnIn($nickname: String!, $password: String!) {
         signIn(nickname: $nickname, password: $password) {
             token
         }
     }
   |}
];

module SignInMutation = ReasonApollo.CreateMutation(SignIn);

type state = {
  errors: option(list(string)),
  nickname: string,
  password: string,
  token: string,
};

type action =
  | Login
  | ChangeNickname(string)
  | ChangePassword(string)
  | GotToken(SignIn.t);

let handleSubmit = (mutation: SignInMutation.apolloMutation, event, self) => {
  let signIn =
    SignIn.make(
      ~nickname=self.ReasonReact.state.nickname,
      ~password=self.ReasonReact.state.password,
      (),
    );
  mutation(~variables=signIn##variables, ()) |> ignore;
  ReactEventRe.Synthetic.preventDefault(event);
  self.ReasonReact.send(Login);
};

let handleNicknameChange = (event, self) =>
  event |> DomHelpers.valueFromEvent |. ChangeNickname |> self.ReasonReact.send;

let handlePasswordChange = (event, self) =>
  event |> DomHelpers.valueFromEvent |. ChangePassword |> self.ReasonReact.send;

let component = ReasonReact.reducerComponent("LoginPage");
let make = _children => {
  ...component,
  initialState: () => {errors: None, nickname: "", password: "", token: ""},
  reducer: (action, state) =>
    switch (action) {
    | Login => ReasonReact.NoUpdate
    | ChangePassword(password) => ReasonReact.Update({...state, password})
    | ChangeNickname(nickname) => ReasonReact.Update({...state, nickname})
    | GotToken(data) =>  {
      switch (data##signIn) {
      | 
      }
    }
    },
  render: ({state, send, handle}) =>
    <SignInMutation>
      ...(
           (mutation, {result}) =>
             switch (result) {
             | Loading => <div> (ReasonReact.string("loading")) </div>
             | Data(data) =>
               send(GotToken(data));
               <div> (ReasonReact.string("gotData")) </div>;
             | Error(error) =>
               <div> (ReasonReact.string(error##message)) </div>
             | _ =>
               <fieldset>
                 <legend>
                   (ReasonReact.string("Log into your account"))
                 </legend>
                 <form onSubmit=(handle(handleSubmit(mutation)))>
                   <div>
                     <label htmlFor="nickname">
                       (ReasonReact.string("Login:"))
                     </label>
                     <input
                       id="nickname"
                       type_="text"
                       inputMode="text"
                       autoComplete="off"
                       onChange=(handle(handleNicknameChange))
                     />
                   </div>
                   <div>
                     <label htmlFor="password">
                       (ReasonReact.string("Password:"))
                     </label>
                     <input
                       value=state.password
                       id="password"
                       type_="password"
                       onChange=(handle(handlePasswordChange))
                     />
                   </div>
                   <input type_="submit" className="submit" value="Login" />
                 </form>
               </fieldset>
             }
         )
    </SignInMutation>,
};